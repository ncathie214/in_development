<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Welcome page route
Route::get('/', function () { return view('welcome'); });

// Recipients routes
Route::get('/recipients', 'RecipientsController@index');
Route::get('recipients/create', 'RecipientsController@create');
Route::post('/recipients', 'RecipientsController@store');
Route::get('recipients/{recipient}', 'RecipientsController@show');

// Login and Register routes
Auth::routes();

// User Dashboard route
Route::get('/home', 'HomeController@index');

// Send Money routes
Route::get('/send', 'SendMoneyController@index');
Route::get('send/create', 'SendMoneyController@create');
Route::post('/send', 'SendMoneyController@store');

// Request Payment routes
Route::get('/request', 'RequestMoneyController@index');
Route::get('request/create', 'RequestMoneyController@create');
Route::post('/request', 'RequestMoneyController@store');

// Profile routes
Route::get('/profile', 'ProfilesController@index');

// Transactions routes
Route::get('/transactions', 'TransactionsController@index');

// Tickets routes
Route::get('/tickets', 'TicketsController@index');
Route::get('tickets/create', 'TicketsController@create');
Route::post('/tickets', 'TicketsController@store');

// Deposit routes
Route::get('/deposit', 'DepositMoneyController@index');
Route::get('deposit/create', 'DepositMoneyController@create');
Route::post('/deposit', 'DepositMoneyController@store');

// Admin routes
Route::get('admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin', 'AdminController@index')->name('admin.dashboard');

Route::get('/adminusers', 'AdminUsersController@index');

Route::get('/admintickets', 'AdminTicketsController@index');

Route::get('/admintransactions', 'AdminTransactionsController@index');

Route::get('/adminearnings', 'AdminEarningsController@index');

Route::get('/adminanalysis', 'AdminAnalysisController@index');

Route::get('/adminemails', 'AdminEmailsController@index');
