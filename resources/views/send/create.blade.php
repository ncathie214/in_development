@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 2em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item active">Send Money</li>
						</ol>
					</div>
	        	</div>

				<div>

					<form role="form" method="POST" action="/send">
						{{ csrf_field() }}

						<div class="row">
							<div class="col-md-6">
								<h3>Send Money</h3>
							</div>
							<div class="col-md-6">
								<div class="alert alert-warning" style="font-family: 'Varela Round';">
									1 UGX : <strong>0.03</strong> KES
								</div>
							</div>
						</div>
						
						<div class="">
							<div class="form-group">
							    <div class="row">
							    	<div class="col-md-6">
							    		<label>Recipient's Country</label>
									    <select class="form-control custom-select" name="recipientCountry">
									        <option value="Kenya">Kenya</option>
											<!-- <option value="Uganda">Uganda</option> -->
											<option value="Rwanda">Rwanda</option>
											<option value="Tanzania">Tanzania</option>
									    </select>	
									</div>
									<div class="col-md-6">
										<label>Sender's Phone Number</label>
										<input id="" type="text" class="form-control" name="senderPhone" value="{{ Auth::user()->phone }}"> 
										<!-- <a href="#" style="color: #2196F3; position: absolute; right: 1em; top: 0;">
											<i class="fa fa-pencil" title="Edit Phone"></i>
										</a> -->
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
								    <div class="col-md-6">
								    	<label>Recipient's Phone Number</label>
								    	<input id="send" type="tel" class="form-control" name="recipientPhone">
								    </div>
								    <div class="col-md-6">
								    	<label>..</label>
										<a href="/recipients/create" class="btn form-control" style="height: 2.8em; color: white; background: #2196F3; padding-top: 0.7em;">Add New Recipient</a>
									</div>
							    </div>
							</div>
							<div class="form-group">
								<div class="row" id="demo">
							    	<div class="col-md-6">
							    		<label>You are sending</label>
								    	<div class="row">
									    	<div class="col-md-8">
									    		<input type="text" class="form-control" id="from_amount" name="from_amount" value="0" placeholder="0" style="border-radius: .25rem 0 0 .25rem;" v-model="from_amount">
											</div>
											<div class="col-md-4">
												<select class="form-control custom-select" name="currencies" v-model="a" style="position: absolute; left: -1em; top: 0; border-radius: 0 .25rem .25rem 0;">
												    <option value="UGX">UGX</option>
												    <!--
												    <option value="KES">KES</option>
													<option value="RWF">RWF</option>
													<option value="TSH">TSH</option> -->
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<label>Recipient Gets</label>
										<div class="row">
											<div class="col-md-8">
												<input type="text" class="form-control" name="to_amount" v-model="to_amount" id="to_amount" value="0" placeholder="0" style="border-radius: .25rem 0 0 .25rem;">
											</div>
											<div class="col-md-4">
												<select class="form-control custom-select" name="currencie1s" v-model="b" style="position: absolute; left: -1em; top: 0; border-radius: 0 .25rem .25rem 0;">
													<option value="KES">KES</option>
													<!--<option value="UGX">UGX</option> -->
													<option value="RWF">RWF</option>
													<option value="TSH">TSH</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
							    <button type="submit" class="btn form-control">
							        Continue
							    </button>
							</div>

							@include('partials.errors')

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

	<div class="container">

		<hr>

		@include('partials.footer')

	</div>

@endsection