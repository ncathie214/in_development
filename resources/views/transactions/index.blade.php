@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection  

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 1.5em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item active">Transactions</li>
						</ol>
					</div>
	        	</div>

				<div class="row">
					<div class="col-md-6">
						<h3>Transactions</h3>
					</div>
					<div class="col-md-6" style="text-align: right;">
					
					</div>
				</div>

				<div>
				    <table class="table">
				          
				        <thead>
				           	<tr>
				               	<th>REF</th>
				               	<th>Type</th>
				                <th>Description</th>
				                <th>Status</th>
				                <th>Date</th>
				            </tr>
				        </thead>

				        <tbody>
				        	@foreach($sends as $send)
				        		<tr>
				        			<td>S00{{ $send->id }}UG</td>
				        			<td>{{ $send->transType }}</td>
				        			<td>You have sent {{ $send->transAmt }} to {{ $send->recipientPhone }}</td>
				        			<td>{{ $send->status }}</td>
				        			<td>{{ $send->created_at->toFormattedDateString() }}</td>
				        		</tr>
				        	@endforeach

				        	@foreach($requests as $request)
				        		<tr>
				        			<td>R00{{ $request->id }}UG</td>
				        			<td>{{ $request->transType }}</td>
				        			<td>You have requested {{ $request->transAmt }} from {{ $request->recipientPhone }}</td>
				        			<td>{{ $request->status }}</td>
				        			<td>{{ $request->created_at->toFormattedDateString() }}</td>
				        		</tr>
				        	@endforeach

				        	@foreach($deposits as $deposit)
				        		<tr>
				        			<td>D00{{ $deposit->id }}UG</td>
				        			<td>{{ $deposit->transType }}</td>
				        			<td>You have deposited {{ $deposit->sendAmt }} into your account</td>
				        			<td>{{ $deposit->status }}</td>
				        			<td>{{ $deposit->created_at->toFormattedDateString() }}</td>
				        		</tr>
				        	@endforeach 
				        </tbody>

				    </table>
				</div>

			</div>

		</div>

	</div>

	<div class="container">
		
		<hr>

		@include('partials.footer')

	</div>

@endsection