<!-- FOOTER -->
<footer>
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2018 Sasula Technologies &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a> &middot; <a href="#">Support</a></p>
</footer>