<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top" style="background: #2196F3;">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}" id="new-logo">
            <img src="/img/logo-blue2.png" alt="App Name" height="40"> App Name
        </a>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                @if (Auth::guest())
                    <li class="nav-item active">
                        <a class="nav-link" href="#why-pesa">Why choose us</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">How it works</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/login') }}">Login</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/register') }}">Create account</a>
                    </li>
                @elseif (Auth::user())
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Welcome, {{ Auth::user()->name }}</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/home">My Account</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @elseif (Auth::admin())
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Welcome Admin, {{ Auth::user()->email }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>