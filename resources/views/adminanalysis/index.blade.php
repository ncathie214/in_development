@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/admin.css">
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2" style="margin-top: -3rem; background: #DBEEFD; border-right: 1px solid #C8E5FC; padding-bottom: 10em;">
                <div style="text-align: center; padding-top: 1em; padding-bottom: 1.5em; border-bottom: 1px solid #C8E5FC; margin-bottom: 1em;">
                    <a class="navbar-brand" href="{{ url('/') }}" id="new-logo" style="color: #2196F3;">
                        <img src="/img/logo-blue.png" alt="App Name" height="40"> App Name
                    </a>
                </div>

                <div>
                    <ul class="nav nav-pills flex-column" id="menu-links">
                        <li class="nav-item">
                            <a class="nav-link" href="/admin">
                            <i class="fa fa-tachometer" aria-hidden="true"></i> 
                            Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/adminusers">
                            <i class="fa fa-users" aria-hidden="true"></i> 
                            Users
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admintickets">
                                <i class="fa fa-comment-o" aria-hidden="true"></i> 
                                Help Desk
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admintransactions">
                            <i class="fa fa-list-ul" aria-hidden="true"></i> 
                            Transactions
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/adminearnings">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>
                                Earnings
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/adminanalysis">
                                <i class="fa fa-line-chart" aria-hidden="true"></i> 
                                Online Analysis
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/adminemails">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                                Emails
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="col-md-10" style="margin-top: -3rem;">
                
                @include('partials.admin-nav')
                <div style="margin-left: 1em; margin-right: 1em;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="step">
                                <div style="margin-bottom: 1em;">
                                    Analysis
                                    <span class="" style="float: right;">
                                        <a href="#" style="color: #5A5A5A">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </a>
                                    </span>
                                </div>
                                <hr>
                                <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
