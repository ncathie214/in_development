@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">

@endsection

@section('content') 

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 1.5em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item"><a href="/tickets">Tickets</a></li>
  							<li class="breadcrumb-item active">New Ticket</li>
						</ol>
					</div>
	        	</div>

				<div>

					<form role="form" method="POST" action="/tickets">
						{{ csrf_field() }}

						<div>
							<h3>New Ticket</h3>
						</div>
						
						<div class="">
							<div class="form-group">
							    <div class="row">
								    <div class="col-md-12">
								    	<label>Subject</label>
								    	<input id="" type="text" class="form-control" name="ticketSubject">
									</div>
									
								</div>
							</div>

							<div class="form-group">
							    <div class="row">
								    <div class="col-md-6">
									    <label>Priority</label>
										<select class="form-control custom-select" name="ticketPriority">
											<option value="Low">Low</option>
											<option value="High">High</option>
											<option value="Medium">Medium</option>
										</select>
									</div>
									<div class="col-md-6">
									    <label>Assign To</label>
										<select class="form-control custom-select" name="ticketAssigne">
											<option value="Technical">Technical</option>
											<option value="Marketing">Marketing</option>
											<option value="Customer Care">Customer care</option>
										</select>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label>Problem</label>
							    <textarea class="form-control" name="ticketBody"></textarea>
							</div>
							<div class="form-group">
							    <button type="submit" class="btn form-control">
							        Submit Ticket
							    </button>
							</div>

							@include('partials.errors')

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

	<div class="container">

		<hr>

		@include('partials.footer')

	</div>

@endsection