@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection  

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 1.5em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item active">Tickets</li>
						</ol>
					</div>
	        	</div>

				<div class="row">
					<div class="col-md-6">
						<h3>Tickets</h3>
					</div>
					<div class="col-md-6" style="text-align: right;">
						<a href="/tickets/create" class="btn deposit-btn">Open a Ticket</a>
					</div>
				</div>

				<div>
					<div class="alert alert-success" role="alert">
						<strong>Trouble using Pesacash?</strong> We are here to help you!
					</div>
				</div>

				<div style="margin-top: 1em;">
					@foreach($tickets as $ticket)
						<div class="ticket-msg">
							<h5>{{ $ticket->ticketSubject }}</h5>
							<hr>
							<p>{{ $ticket->ticketBody }}</p>
						</div>
					@endforeach
				</div>

			</div>

		</div>

	</div>

	<div class="container">
		
		<hr>

		@include('partials.footer')

	</div>

@endsection