@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 2em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item active">Deposit Money</li>
						</ol>
					</div>
	        	</div>

				<div>

					<form role="form" method="POST" action="">
						{{ csrf_field() }}

						<div>
							<h3>Deposit Money</h3>
						</div>
						
						<div class="">
							<div class="form-group">
							    <div class="row">
							    	<div class="col-md-6">
							    		<label>Phone Number</label>
								    	<input id="" type="text" class="form-control" name="senderPhone" value="{{ Auth::user()->phone }}">
										<!--<a href="#" style="color: #2196F3; position: absolute; right: 1em; top: 0;">
											<i class="fa fa-pencil" title="Edit Phone"></i>
										</a> -->
									</div>
									<div class="col-md-6">
								    	<label>Amount to Deposit</label>
									    <div class="row">
									    	<div class="col-md-8">
									    		<input id="" type="text" class="form-control" name="sendAmt" value="0" style="border-radius: .25rem 0 0 .25rem;">
								    		</div>
								    		<div class="col-md-4">
								    			<select class="form-control custom-select" name="sendAmtCurrency" style="position: absolute; left: -1em; top: 0; border-radius: 0 .25rem .25rem 0;">
													<option value="UGX">UGX</option>
													<option value="KES">KES</option>
													<option value="RWF">RWF</option>
													<option value="TSH">TSH</option>
												</select>
								    		</div>
								    	</div>
								    </div>
								</div>
							</div>
							<div class="form-group">
							    <button type="submit" class="btn form-control" data-toggle="modal" data-target="#flipFlop">
							        Deposit Money
							    </button>

							    <!-- The modal -->
								<div class="modal fade" id="flipFlop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="modalLabel">Flip-flop</h4>
											</div>
											<div class="modal-body">
												A type of open-toed sandal.
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>

							</div>

							@include('partials.errors')

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

	<div class="container">

		<hr>

		@include('partials.footer')

	</div>

	<!-- Initialize Bootstrap functionality -->
	<script>
		// Initialize tooltip component
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})

		// Initialize popover component
		$(function () {
		  $('[data-toggle="popover"]').popover()
		})
	</script>

@endsection