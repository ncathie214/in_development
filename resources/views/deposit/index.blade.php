@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 2em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item active">Deposit Money</li>
						</ol>
					</div>
	        	</div>

				<div>

					<form role="form" method="POST" action="">
						{{ csrf_field() }}

						<div>
							<h3>Thank you for Depositing!</h3>
						</div>
						
						<div class="">
							Your account balance will updated soon!
						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

	<div class="container">

		<hr>

		@include('partials.footer')

	</div>

@endsection