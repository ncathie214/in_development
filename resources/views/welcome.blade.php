@extends('layouts.master')

@section('css')

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

    @include('partials.nav')

    <div class="introduction">
        <div class="container">
            <div>
                <h3>
                    Send and Recieve Money across
                    East Africa 
                    At ZERO Fees.
                </h3>
            </div>
            <div class="wrap-sec-1">
                <form class="form">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="labels">Where do you want to send money to?</label>
                                <select class="form-control custom-select" style="height: 2.8em">
                                    <option>Uganda</option>
                                    <option>Kenya</option>
                                    <option>Rwanda</option>
                                    <option>Tanzania</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label id="labels">You are Sending</label>
                                <input class="form-control" type="text" name="sending" value="0">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label id="labels">Recipient Gets</label>
                                <input class="form-control" type="text" name="receiving" value="0">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label id="labels">Need Help?</label>
                                <button class="form-control btn" style="background: #FF5722; color: white;">Get Started</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="why-pesa">
        <div class="container">
            <div style="margin-bottom: 5em; text-align: center; padding-top: 4rem; font-family: 'Varela Round';">
                <h2>Why <span>Choos</span>e Us?</h2>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service-icon rounded-circle">
                                <i class="fa fa-rocket fa-2x" aria-hidden="true" style="color: white;"></i>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h5 style="font-family: 'Varela Round';">Instant Transfer</h5>
                            <p>Recieve money in seconds. You don't have to wait for hours or days to recieve money.</p>
                        </div>
                    </div>
                </div><!-- /.col-lg-4 -->
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service-icon rounded-circle">
                                <i class="fa fa-exchange fa-2x" aria-hidden="true" style="color: white"></i>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h5 style="font-family: 'Varela Round';">Low Exchange Rates</h5>
                            <p>Our exchange rates are incomparable to other exchange services.</p>
                        </div>
                    </div>
                </div><!-- /.col-lg-4 -->
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service-icon rounded-circle">
                                <i class="fa fa-clock-o fa-2x" aria-hidden="true" style="color: white"></i>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h5 style="font-family: 'Varela Round';">Available 24/7</h5>
                            <p>Send Money any time of the day. Be it night or day, we are always open.</p>
                        </div>
                    </div>
                </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service-icon rounded-circle">
                                <i class="fa fa-ban fa-2x" aria-hidden="true" style="color: white;"></i>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h5 style="font-family: 'Varela Round';">No Charges</h5>
                            <p>Send more money at absolutely no cost. You dont have to budget for the charges too.</p>
                        </div>
                    </div>
                </div><!-- /.col-lg-4 -->
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service-icon rounded-circle">
                                <i class="fa fa-check fa-2x" aria-hidden="true" style="color: white"></i>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h5 style="font-family: 'Varela Round';">Convinient</h5>
                            <p>Cash out using any mobile wallet. We support all mobile wallets, so you can choose your favorite.</p>
                        </div>
                    </div>
                </div><!-- /.col-lg-4 -->
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service-icon rounded-circle"">
                                <i class="fa fa-lock fa-2x" aria-hidden="true" style="color: white"></i>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h5 style="font-family: 'Varela Round';">Safe and Secure</h5>
                            <p>Your money is always safe. We apply an OTP everytime your account is accesed and for every transaction made.</p>
                        </div>
                    </div>
                </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->
        </div>
    </div>

    <div id="how-it-works">
        <div class="container">
            <div style="margin-bottom: 6em; text-align: center; padding-top: 4rem; font-family: 'Varela Round';">
                <h2>How <span>it W</span>orks</h2>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1" style="text-align: center;">
                    <img src="/img/howitworks.png" alt="how it works">
                </div>
            </div>
        </div>
    </div>

    <div id="about-pesa" style="background: #DBEEFD;">
        <div class="container">
            <div style="padding-bottom: 6em; text-align: center; padding-top: 4rem; font-family: 'Varela Round';">
                <h2>About Us</h2>
            </div>
        </div>
    </div>

    <div id="contact us" style="background: white;">
        <div class="container">
            <div style="padding-bottom: 6em; text-align: center; padding-top: 4rem; font-family: 'Varela Round';">
                <h2>Contact Us</h2>
            </div>
        </div>
    </div>

    <div id="welcome-footer" style="background: #0A6EBD; color: white;">
        <div class="container">
            @include('partials.footer')
        </div>
    </div>

@endsection

