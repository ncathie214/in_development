@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/admin.css">
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2" style="margin-top: -3rem; background: #DBEEFD; border-right: 1px solid #C8E5FC; padding-bottom: 10em;">
                <div style="text-align: center; padding-top: 1em; padding-bottom: 1.5em; border-bottom: 1px solid #C8E5FC; margin-bottom: 1em;">
                    <a class="navbar-brand" href="{{ url('/') }}" id="new-logo" style="color: #2196F3;">
                        <img src="/img/logo-blue.png" alt="App Name" height="40"> App Name
                    </a>
                </div>

                <div>
                    <ul class="nav nav-pills flex-column" id="menu-links">
                        <li class="nav-item">
                            <a class="nav-link" href="/admin">
                                <i class="fa fa-tachometer" aria-hidden="true"></i> 
                                Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/adminusers">
                                <i class="fa fa-users" aria-hidden="true"></i> 
                                Users
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admintickets">
                                <i class="fa fa-comment-o" aria-hidden="true"></i> 
                                Help Desk
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admintransactions">
                                <i class="fa fa-list-ul" aria-hidden="true"></i> 
                                Transactions
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/adminearnings">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>
                                Earnings
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/adminanalysis">
                                <i class="fa fa-line-chart" aria-hidden="true"></i> 
                                Online Analysis
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/adminemails">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                                Emails
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-10" style="margin-top: -3rem;">
                
                @include('partials.admin-nav')

                <div style="margin-left: 1em; margin-right: 1em;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="step">
                                <div style="margin-bottom: 1em;">
                                    Manage Users
                                    <span class="" style="float: right;">
                                        <a href="#" style="color: #5A5A5A">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </a>
                                    </span>
                                </div>
                                <hr>
                                <div class="">
                                    <div class="table-responsive">
                                        <div id="" class="no-footer">
                                            <div class="row" style="margin-top: 0.5em; margin-bottom: 0.5em;">
                                                <div class="col-md-6">
                                                    <div class="" id="">
                                                        <label style="display: inline-flex; justify-content: flex-start;">
                                                            Show  
                                                            <select name="" class="custom-select form-control" style="margin-left: 1em; margin-right: 1em; margin-top: -0.5em; font-family: 'Varela Round';">
                                                                <option value="10">10</option>
                                                                <option value="25">25</option>
                                                                <option value="50">50</option>
                                                                <option value="100">100</option>
                                                            </select>  
                                                            entries
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="text-align: right;">
                                                    <div id="" class="">
                                                        <label style="display: inline-flex;">
                                                            Search:
                                                            <input class="form-control input-sm" aria-controls="table" type="search" style="margin-left: 1em; margin-top: -0.5em; font-family: 'Varela Round';">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-bordered no-footer" id="table" role="grid" aria-describedby="table_info" style="font-size: 0.9em;">
                                                        <thead>
                                                            <tr class="filters" role="row">
                                                                <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 208.333px;" aria-sort="ascending" aria-label="First Name: activate to sort column descending">
                                                                    Name
                                                                </th>
                                                                <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 96.3333px;" aria-label="Last Name: activate to sort column ascending">
                                                                    Phone
                                                                </th>
                                                                <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 98.3333px;" aria-label="User E-mail: activate to sort column ascending">
                                                                    Country
                                                                </th>
                                                                <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 60.3333px;" aria-label="Status: activate to sort column ascending">
                                                                    Status
                                                                </th>
                                                                <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 166.3333px;" aria-label="Created At: activate to sort column ascending">
                                                                    Created At
                                                                </th>
                                                                <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 69px;" aria-label="Actions: activate to sort column ascending">
                                                                    Actions
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody> 
                                                            @foreach($users as $user)
                                                                <tr role="row" class="odd">
                                                                    <td class="sorting_1">{{ $user->name }}</td>
                                                                    <td>{{ $user->phone }}</td>
                                                                    <td>{{ $user->country }}</td>
                                                                    <td>Activate</td>
                                                                    <td>{{ $user->created_at->toFormattedDateString() }}</td>
                                                                    <td style="display: flex; justify-content: space-around;">
                                                                        <a href="#" style="color: #879FCE;"><i class="fa fa-pencil" title="Edit User"></i></a>
                                                                        <a href="#" style="color: #FF6A71;" data-toggle="modal" data-target="#delete"><i class="fa fa-close" title="Delete User"></i></a>
                                                                        <a href="#" style="color: #6ACC99;"><i class="fa fa-eye" title="View User"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row" style="font-size: 0.9em;">
                                                <div class="col-md-5">
                                                    <div class="" id="table_info" role="status" aria-live="polite">
                                                        Showing 1 to {{ $user_counts }} of {{ $user_counts }} entries
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="" id="table_paginate">
                                                        <ul class="pagination justify-content-end">
                                                            <li class="page-item">
                                                                <a class="page-link" href="#" aria-label="Previous">
                                                                    <span aria-hidden="true">&laquo;</span>
                                                                    <span class="sr-only">Previous</span>
                                                                </a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a href="#" class="page-link">1</a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a href="#" class="page-link">2</a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a href="#" class="page-link">3</a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a href="#" class="page-link">4</a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a href="#" class="page-link">5</a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a href="#" class="page-link">6</a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a href="#" class="page-link">7</a>
                                                            </li>
                                                            <li class="page-item">
                                                                <a class="page-link" href="#" aria-label="Next">
                                                                    <span aria-hidden="true">&raquo;</span>
                                                                    <span class="sr-only">Next</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
