@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 1.5em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item active">Recipients</li>
						</ol>
					</div>
	        	</div>

				<div class="row">
					<div class="col-md-6">
						<h3>Recipients</h3>
					</div>
					<div class="col-md-6" style="text-align: right;">
						<a href="/recipients/create" class="btn deposit-btn">Add New Recipient</a>
					</div>
				</div>

				<div>
				    <table class="table">
				          
				        <thead>
				           	<tr>
				               	<th>Name</th>
				                <th>Phone No.</th>
				                <th>Country</th>
				                <th>Actions</th>
				            </tr>
				        </thead>

				        <tbody>

				        	@foreach($recipients as $recipient)

					            <tr>
					                <td>{{ $recipient->contactName }}</td>
					                <td>{{ $recipient->contactPhone }}</td>
					                <td>{{ $recipient->contactCountry }}</td>
					                <td>
					                	<a href="#" style="margin-right: 1em; color: #2196F3;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
					                	<a href="/recipients/{{ $recipient->id }}" style="margin-right: 1em; color: #2196F3;"><i class="fa fa-eye" aria-hidden="true"></i></a>
					                	<a href="#" style="color: #2196F3;"><i class="fa fa-times" aria-hidden="true"></i></a>
					                </td>
					            </tr>

				            @endforeach

				        </tbody>

				    </table>
				</div>

			</div>

		</div>

	</div>

	<div class="container">
		
		<hr>

		@include('partials.footer')

	</div>

@endsection