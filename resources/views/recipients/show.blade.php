@extends('layouts.master')

@section('css') 

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
			        <div class="col-md-12">
				        <ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 1.5em;">
							<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
		  					<li class="breadcrumb-item"><a href="/recipients">Recipients</a></li>
		  					<li class="breadcrumb-item active">Details</li>
						</ol>
					</div>
			    </div>

				<div>
					<h3>Recipient Details</h3>
				</div>

				<div class="details-card">
					<div class="row">
						<div class="col-md-4">
							<div class="profile-img rounded-circle">
								NG
							</div>
						</div>
						<div class="col-md-8" style="border-left: 1px solid #ddd; padding-left: 3em">
							<div style="padding-bottom: .5em;">{{ $recipient->contactName }}</div>
							<div style="padding-bottom: .5em;">{{ $recipient->contactPhone }}</div>
							<div style="padding-bottom: .5em;">{{ $recipient->contactCountry }}</div>
							<div>
								<a href="#" class="btn edit-btn">
									<i class="fa fa-pencil" aria-hidden="true"></i> 
									Edit Contact
								</a>
					            <a href="#" class="btn delete-btn">
					            	<i class="fa fa-times" aria-hidden="true"></i> 
					            	Delete Contact
					            </a>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

	<div class="container">
		
		<hr>

		@include('partials.footer')

	</div>

@endsection