@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">

@endsection

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 1.5em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item"><a href="/recipients">Recipients</a></li>
  							<li class="breadcrumb-item active">New Recipient</li>
						</ol>
					</div>
	        	</div>

				<div>

					<form role="form" method="POST" action="/recipients">
						{{ csrf_field() }}

						<div>
							<h3>New Recipient</h3>
						</div>
						
						<div class="">
							<div class="form-group">
							    <div class="row">
								    <div class="col-md-6">
								    	<input id="" type="text" class="form-control" name="contactName" placeholder="Full Name">
									</div>
									<div class="col-md-6">
										<input id="" type="text" class="form-control" name="contactPhone" placeholder="Phone No.">
									</div>
								</div>
							</div>
							<div class="form-group">
							    <select class="form-control custom-select" name="contactCountry">
							        <option value="Kenya">Kenya</option>
									<option value="Uganda">Uganda</option>
									<option value="Rwanda">Rwanda</option>
									<option value="Tanzania">Tanzania</option>
							    </select>
							</div>
							<div class="form-group">
							    <button type="submit" class="btn form-control">
							        Add Recipient
							    </button>
							</div>

							@include('partials.errors')

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

	<div class="container">

		<hr>

		@include('partials.footer')

	</div>

@endsection