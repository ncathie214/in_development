@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/recipients.css')}}" rel="stylesheet">
	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">

@endsection

@section('content')

	@include('partials.nav')

	<div class="container" id="recipient_view">

		<div class="row">

			@include('partials.account')

			<div class="col-md-8">

				<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em; margin-bottom: 2em;">
						  	<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
  							<li class="breadcrumb-item active">Request Payment</li>
						</ol>
					</div>
	        	</div>

				<div>

					<form role="form" method="POST" action="/request">
						{{ csrf_field() }}

						<div>
							<h3>Request Payment</h3>
						</div>
						
						<div class="">
							<div class="form-group">
							    <div class="row">
							    	<div class="col-md-6">
								    	<div class="row">
									    	<div class="col-md-8">
									    		<label>You are requesting</label>
										    	<input type="text" class="form-control" name="transAmt" value="0" style="border-radius: .25rem 0 0 .25rem;">
											</div>
											<div class="col-md-4">
											    <select class="form-control custom-select" name="recipientCountry" style="position: absolute; left: -1em; top: 1.95em; border-radius: 0 .25rem .25rem 0;">
											        <option value="KES">KES</option>
													<option value="UGX">UGX</option>
													<option value="RWF">RWF</option>
													<option value="TSH">TSH</option>
											    </select>
										    </div>
									    </div>
								    </div>
								    <div class="col-md-6">
							    		<label>Contact Number</label>
								    	<input id="" type="text" class="form-control" name="recipientPhone">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
								    <div class="col-md-12">
								    	<label>Reason for payment</label>
								    	<textarea class="form-control" name="notes" placeholder="Enter Message"></textarea>
									</div>
							    </div>
							</div>
							<div class="form-group">
							    <button type="submit" class="btn form-control">
							        Send Request
							    </button>
							</div>

							@include('partials.errors')

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

	<div class="container">

		<hr>

		@include('partials.footer')

	</div>

@endsection