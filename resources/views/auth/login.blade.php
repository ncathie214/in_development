@extends('layouts.master')

@section('css')

    <link href="{{ asset('css/register.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

    @include('partials.nav')

    <div class="container" id="register_view">
        
        <div class="row">

            @include('partials.slide')

            <div class="col-md-6">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <h3 style="text-align: center; padding-bottom: 1em; font-family: 'Varela Round';">
                        Log into Account
                    </h3>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </div>
                                <input placeholder="Phone Number" id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>
                            </div>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </div>
                                <input placeholder="Password" id="password" type="password" class="form-control" name="password" required autocomplete="off">
                                <input type="button" id="togglePasswordField" value="Show" style="display:none; background-color: #5CB3FD; color: white; border-top: 1px solid #5CB3FD; border-bottom: 1px solid #5CB3FD; border-right: 1px solid #5CB3FD; border-left: none; border-radius: 0 .25rem .25rem 0; padding-left: 1em; padding-right: 1em;" />
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col">
                            <div class="checkbox">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Remember Me</span>
                                  </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        
                        <div class="col">

                            <div class="row">

                                <div class="col-md-6">
                                    <button type="submit" class="btn form-control">
                                        Login
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>

                </form>

                <div style="text-align: center; margin-bottom: 2em; margin-top: 2em;">
                    <img src="img/divider.png" height="40">
                </div>

                <div class="col" style="padding-left: 4.8em; padding-right: 4.8em;">
                    <button class="form-control btn" style="box-shadow: 0 0 1em #c1e4ff; background: #3b5998; border: 1px solid #3b5998;">
                        <i class="fa fa-facebook aria-hidden="true" style="padding-right: 2em"></i> 
                        Connect with Facebook
                    </button>
                </div>

            </div>

        </div>

    </div>

    <div class="container">
        
        <hr>

        @include('partials.footer')

    </div>

    <script>
        
        (function() {

            try {

                // switch the password field to text, then back to password to see if it supports
                // changing the field type (IE9+, and all other browsers do). then switch it back.
                var password = document.getElementById('password');
                password.type = 'text';
                password.type = 'password';
                
                // if it does support changing the field type then add the event handler and make
                // the button visible. if the browser doesn't support it, then this is bypassed
                // and code execution continues in the catch() section below
                var togglePasswordField = document.getElementById('togglePasswordField');
                togglePasswordField.addEventListener('click', togglePasswordFieldClicked, false);
                togglePasswordField.style.display = 'inline';
                
            }
            catch(err) {

            }

        })();

        function togglePasswordFieldClicked() {

            var password = document.getElementById('password');
            var value = password.value;

            if(password.type == 'password') {
                password.type = 'text';
            }
            else {
                password.type = 'password';
            }
            
            password.value = value;

        }

    </script>

@endsection
