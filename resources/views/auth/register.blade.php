@extends('layouts.master')

@section('css')

    <link href="{{ asset('css/register.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

    @include('partials.nav')

    <div class="container" id="register_view">

        <div class="row">

            @include('partials.slide')

            <div class="col-md-6" style="">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <h3 style="text-align: center; padding-bottom: 1em; font-family: 'Varela Round';">
                        Create New Account
                    </h3>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                                </div>
                                <input id="name" type="text" class="form-control" name="name" placeholder="Full Name" value="{{ old('name') }}" required autofocus>
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-globe" aria-hidden="true"></i>
                                </div>
                                <select class="form-control custom-select" name="country">
                                    <option value="Uganda">Uganda</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Tanzania">Tanzania</option>
                                </select>
                            </div>
                            @if ($errors->has('country'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('country') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </div>
                                <input id="phone" placeholder="Phone Number" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>
                            </div>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </div>
                                <input id="password" placeholder="Password" type="password" class="form-control" name="password" required autocomplete="off">
                                <input type="button" id="togglePasswordField" value="Show" style="display:none; background-color: #5CB3FD; color: white; border-top: 1px solid #5CB3FD; border-bottom: 1px solid #5CB3FD; border-right: 1px solid #5CB3FD; border-left: none; border-radius: 0 .25rem .25rem 0; padding-left: 1em; padding-right: 1em;" />
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon"  style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </div>
                                <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <div class="col">
                            <button type="submit" class="btn btn-primary form-control">
                                Create Account
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <div class="container">
        
        <hr>

        @include('partials.footer')

    </div>

    <script>
        
        (function() {

            try {

                // switch the password field to text, then back to password to see if it supports
                // changing the field type (IE9+, and all other browsers do). then switch it back.
                var password = document.getElementById('password');
                password.type = 'text';
                password.type = 'password';
                
                // if it does support changing the field type then add the event handler and make
                // the button visible. if the browser doesn't support it, then this is bypassed
                // and code execution continues in the catch() section below
                var togglePasswordField = document.getElementById('togglePasswordField');
                togglePasswordField.addEventListener('click', togglePasswordFieldClicked, false);
                togglePasswordField.style.display = 'inline';
                
            }
            catch(err) {

            }

        })();

        function togglePasswordFieldClicked() {

            var password = document.getElementById('password');
            var value = password.value;

            if(password.type == 'password') {
                password.type = 'text';
            }
            else {
                password.type = 'password';
            }
            
            password.value = value;

        }

    </script>



@endsection
