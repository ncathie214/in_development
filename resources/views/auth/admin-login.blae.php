@extends('layouts.master')

@section('css')

    <link href="{{ asset('css/register.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

    <div class="container" id="register_view_admin">
        
        <div class="row">

            <div class="col-md-6 offset-md-3">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                    {{ csrf_field() }}

                    <div style="text-align: center; margin-bottom: 1em;">
                        <img src="/img/logo-blue.png" height="100">
                    </div>
                    
                    <h3 style="text-align: center; padding-bottom: 1em; font-family: 'Varela Round';">
                        Admin Login
                    </h3>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </div>
                                <input placeholder="Phone Number" id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>
                            </div>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-addon" style="background: white; width: 3.2em; color: #b7bcc0;">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </div>
                                <input placeholder="Password" id="password" type="password" class="form-control" name="password" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col">
                            <div class="checkbox">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Remember Me</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        
                        <div class="col">

                            <div class="row">

                                <div class="col-md-6">
                                    <button type="submit" class="btn form-control">
                                        Login
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>

                </form>

            </div>

        </div>

    </div>

@endsection
