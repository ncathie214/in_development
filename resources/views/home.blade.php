@extends('layouts.master')

@section('css')

	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('content')

    @include('partials.nav')

    <div class="container" id="dashboard_view">

    	<div class="row">

	        @include('partials.account')

	        <div class="col-md-8">
	        	<div class="row">
	        		<div class="col-md-12">
		        		<ol class="breadcrumb" style="margin-top: 2em; font-size: 0.9em">
						  	<li class="breadcrumb-item active">Dashboard</li>
						</ol>
					</div>
	        	</div>
	        	<div class="row">
	        		<div class="col-md-4">
	        			<div class="elements">
                            <a href="/send/create">
                                <i class="fa fa-paper-plane fa-3x" aria-hidden="true" style="padding-bottom: 20px;"></i><br>
                                Send Money
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="elements">
                            <a href="/request/create">
                                <i class="fa fa-credit-card fa-3x" aria-hidden="true" style="padding-bottom: 20px;"></i><br>
                                Request Payment
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4">
	        			<div class="elements">
                            <a href="/recipients">
                                <i class="fa fa-users fa-3x" aria-hidden="true" style="padding-bottom: 20px;"></i><br>
                                My Recipients
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">

	        		<div class="col-md-4">
	        			<div class="elements">
                            <a href="/transactions">
                                <i class="fa fa-list-ul fa-3x" aria-hidden="true" style="padding-bottom: 20px;"></i><br>
                                My Transactions
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="elements">
                            <a href="/profile">
                                <i class="fa fa-user fa-3x" aria-hidden="true" style="padding-bottom: 20px;"></i><br>
                                My Profile
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="elements">
                            <a href="/tickets">
                                <i class="fa fa-comments-o fa-3x" aria-hidden="true" style="padding-bottom: 20px;"></i><br>
                                Support Center 
                            </a>
                        </div>
                    </div>

                </div>

	        </div>

	    </div>

    </div>

    <div class="container">
        
        <hr>

        @include('partials.footer')

    </div>

@endsection


