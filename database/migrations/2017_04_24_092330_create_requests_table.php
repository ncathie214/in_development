<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_moneys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id'); 
            $table->string('transType')->default('request');
            $table->integer('transAmt');
            $table->string('recipientCountry');
            $table->string('recipientPhone');
            $table->text('notes');
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_moneys');
    }
}
