<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sends', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('transType')->default('send');
            $table->string('recipientPhone');
            $table->string('recipientCountry');
            $table->integer('sendAmt');
            $table->string('sendAmtCurrency');
            $table->integer('recievedAmt');
            $table->string('recievedAmtCurrency');
            $table->string('senderPhone');
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sends');
    }
}
