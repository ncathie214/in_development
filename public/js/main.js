var demo = new Vue({
    el: '#demo',
    data: function(){
    	return {
		  from_amount: '',
            to_amount: '',
      };
    },
    watch: {
        from_amount: function(value) {
            this.to_amount = value * 0.03
        },
        to_amount: function(value) {
            this.from_amount = value / 0.03
        }
    }
})