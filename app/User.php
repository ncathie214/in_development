<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'country', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function recipients()
    {
        return $this->hasMany(Recipient::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function sends()
    {
        return $this->hasMany(Send::class);
    }

    public function request_moneys()
    {
        return $this->hasMany(RequestMoney::class);
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class);
    }

}
