<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestMoney extends Model
{
    protected $fillable = [
    	'transAmt',
    	'recipientCountry',
    	'recipientPhone',
    	'notes',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}