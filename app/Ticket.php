<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
    	'ticketSubject', 
    	'ticketPriority', 
    	'ticketAssigne', 
    	'ticketBody'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
