<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Send extends Model
{
    protected $fillable = [
        'user_id',
    	'recipientPhone',
    	'recipientCountry',
    	'sendAmt',
        'sendAmtCurrency',
        'recievedAmt',
        'recievedAmtCurrency',
    	'senderPhone',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}