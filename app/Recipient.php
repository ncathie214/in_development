<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    protected $fillable = [
    	'user_id',
    	'contactName', 
    	'contactPhone', 
    	'contactCountry',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

}