<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $fillable = [
    	'user_id',
    	'sendAmt',
    	'sendAmtCurrency',
    	'senderPhone',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}